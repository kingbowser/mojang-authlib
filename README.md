Mojang's Authlib 1.4.2
======================

This is a fully working and compilable copy of AuthLib version 1.4.2.

This software was written by Mojang AB, the sole proprietor of AuthLib.
Mojang AB reserves all rights to this software.

_License Restrictions:_
------------------------
1. ***DO NOT FUCKING REDISTRIBUTE THIS.***
2. ***DO NOT FUCKING REDISTRIBUTE THIS VIA MAVEN CENTRAL.***
3. ***Do not reproduce this source code in its current state.***
4. ***Do not sell this.***

------------------------------------------------------------------------------------------------------------------------

How do I build this?
====================
_You must have [Gradle](http://gradle.org) installed to build this!_

Simply run `gradle jar` in the project folder. You will find `authlib.jar` in `./build`.