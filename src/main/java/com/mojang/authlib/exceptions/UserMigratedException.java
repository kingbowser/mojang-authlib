package com.mojang.authlib.exceptions;

/**
 * Exception thrown when a player attempts to log in with legacy (minecraft) credentials that have been migrated to the new authentication scheme
 */
public class UserMigratedException
        extends InvalidCredentialsException {
    public UserMigratedException() {
    }

    public UserMigratedException(String message) {
        super(message);
    }

    public UserMigratedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserMigratedException(Throwable cause) {
        super(cause);
    }
}

