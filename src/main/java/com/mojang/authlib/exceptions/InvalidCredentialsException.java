package com.mojang.authlib.exceptions;

/**
 * Exception thrown when the user provides invalid account credentials
 */
public class InvalidCredentialsException
        extends AuthenticationException {
    public InvalidCredentialsException() {
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCredentialsException(Throwable cause) {
        super(cause);
    }
}

