package com.mojang.authlib.exceptions;

/**
 * Exception that is thrown when a general error occurs during authentication
 */
public class AuthenticationException
        extends Exception {
    public AuthenticationException() {
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationException(Throwable cause) {
        super(cause);
    }
}

