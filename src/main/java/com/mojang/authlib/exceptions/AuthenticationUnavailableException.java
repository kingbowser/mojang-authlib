package com.mojang.authlib.exceptions;

/**
 * Exception that is thrown when an AuthenticationService cannot be reached or is not responding to requests
 */
public class AuthenticationUnavailableException
        extends AuthenticationException {
    public AuthenticationUnavailableException() {
    }

    public AuthenticationUnavailableException(String message) {
        super(message);
    }

    public AuthenticationUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationUnavailableException(Throwable cause) {
        super(cause);
    }
}

