package com.mojang.authlib;

import com.mojang.authlib.minecraft.MinecraftSessionService;

/**
 * Contract for a remote service that provides a minecraft authentication service and a user authentication service
 */
public interface AuthenticationService {
    public UserAuthentication createUserAuthentication(Agent var1);

    public MinecraftSessionService createMinecraftSessionService();
}

