package com.mojang.authlib.legacy;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.HttpAuthenticationService;
import com.mojang.authlib.HttpUserAuthentication;
import com.mojang.authlib.UserType;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * User
 */
public class LegacyUserAuthentication
     extends HttpUserAuthentication {

    /**
     * Path the user remote authentication service base
     */
    private static final URL AUTHENTICATION_URL = HttpAuthenticationService.constantURL("https://login.minecraft.net");

    /**
     * Protocol Version
     */
    private static final int AUTHENTICATION_VERSION = 14;

    private static final int RESPONSE_PART_PROFILE_NAME     = 2;
    private static final int RESPONSE_PART_SESSION_TOKEN    = 3;
    private static final int RESPONSE_PART_PROFILE_ID       = 4;

    /**
     * Session token
     */
    private String sessionToken;

    /**
     * Bridge constructor.
     *
     * @param authenticationService Legacy AuthenticationService implementation
     */
    protected LegacyUserAuthentication(LegacyAuthenticationService authenticationService) {
        super(authenticationService);
    }

    /** {@inheritDoc} */
    @Override
    public void logIn() throws AuthenticationException {
        String response;
        if (StringUtils.isBlank(this.getUsername())) {
            throw new InvalidCredentialsException("Invalid username");
        }
        if (StringUtils.isBlank(this.getPassword())) {
            throw new InvalidCredentialsException("Invalid password");
        }
        HashMap<String, Object> args = new HashMap<>();
        args.put("user", this.getUsername());
        args.put("password", this.getPassword());
        args.put("version", AUTHENTICATION_VERSION);
        try {
            response = this.getAuthenticationService().performPostRequest(LegacyUserAuthentication.AUTHENTICATION_URL, HttpAuthenticationService.buildQuery(args), "application/x-www-form-urlencoded").trim();
        } catch (IOException e) {
            throw new AuthenticationException("Authentication server is not responding", e);
        }
        String[] split = response.split(":");
        if (split.length != 5) throw new InvalidCredentialsException(response);
        String profileId    = split[RESPONSE_PART_PROFILE_ID];
        String profileName  = split[RESPONSE_PART_PROFILE_NAME];
        String sessionToken = split[RESPONSE_PART_SESSION_TOKEN];
        if (StringUtils.isBlank(profileId) || StringUtils.isBlank(profileName) || StringUtils.isBlank(sessionToken)) {
            throw new AuthenticationException("Unknown response from authentication server: " + response);
        }
        this.setSelectedProfile(new GameProfile(profileId, profileName));
        this.sessionToken = sessionToken;
        this.setUserType(UserType.LEGACY);
    }

    /** {@inheritDoc} */
    @Override
    public void logOut() {
        super.logOut();
        this.sessionToken = null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean canPlayOnline() {
        return this.isLoggedIn() && this.getSelectedProfile() != null && this.getAuthenticatedToken() != null;
    }

    /** {@inheritDoc} */
    @Override
    public GameProfile[] getAvailableProfiles() {
        if (this.getSelectedProfile() == null) return new GameProfile[0];
        return new GameProfile[]{this.getSelectedProfile()};
    }

    /** {@inheritDoc} */
    @Override
    public void selectGameProfile(GameProfile gameProfile) throws AuthenticationException {
        throw new UnsupportedOperationException("Game profiles cannot be changed in the legacy authentication service");
    }

    /** {@inheritDoc} */
    @Override
    public String getAuthenticatedToken() {
        return this.sessionToken;
    }

    /** {@inheritDoc} */
    @Override
    public String getUserID() {
        return this.getUsername();
    }

    /** {@inheritDoc} */
    @Override
    public LegacyAuthenticationService getAuthenticationService() {
        return (LegacyAuthenticationService) super.getAuthenticationService();
    }
}

