package com.mojang.authlib.legacy;

import com.mojang.authlib.Agent;
import com.mojang.authlib.HttpAuthenticationService;
import org.apache.commons.lang3.Validate;

import java.net.Proxy;

/**
 * AuthenticationService implementation for legacy (minecraft) account credentials
 */
public class LegacyAuthenticationService
     extends HttpAuthenticationService {

    /**
     * Bridge constructor.
     *
     * @see com.mojang.authlib.HttpAuthenticationService(java.net.Proxy)
     * @param proxy network proxy to be used when connecting to the remote service
     */
    protected LegacyAuthenticationService(Proxy proxy) {
        super(proxy);
    }

    @Override
    /** {@inheritDoc} */
    public LegacyUserAuthentication createUserAuthentication(Agent agent) {
        Validate.notNull((Object) agent);
        if (agent == Agent.MINECRAFT) return new LegacyUserAuthentication(this);
        throw new IllegalArgumentException("Legacy authentication cannot handle anything but Minecraft");
    }

    @Override
    /** {@inheritDoc} */
    public LegacyMinecraftSessionService createMinecraftSessionService() {
        return new LegacyMinecraftSessionService(this);
    }
}

