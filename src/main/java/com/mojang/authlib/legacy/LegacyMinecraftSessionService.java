package com.mojang.authlib.legacy;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.HttpAuthenticationService;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.minecraft.HttpMinecraftSessionService;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * MinecraftSessionService implementation for legacy (minecraft) accounts
 */
public class LegacyMinecraftSessionService
     extends HttpMinecraftSessionService {

    /**
     * URL to the base of the remote service
     */
    private static final String BASE_URL = "http://session.minecraft.net/game/";

    /**
     * URL to the remote service call made when joining servers
     */
    private static final URL JOIN_URL = HttpAuthenticationService.constantURL("http://session.minecraft.net/game/joinserver.jsp");

    /**
     * URL to the remote service call made when checking to see that a player has joined a server
     */
    private static final URL CHECK_URL = HttpAuthenticationService.constantURL("http://session.minecraft.net/game/checkserver.jsp");

    /**
     * Bridge constructor.
     *
     * @see com.mojang.authlib.minecraft.HttpMinecraftSessionService(com.mojang.authlib.HttpAuthenticationService)
     * @param authenticationService LegacyAuthenticationService implementation
     */
    protected LegacyMinecraftSessionService(LegacyAuthenticationService authenticationService) {
        super(authenticationService);
    }

    /** {@inheritDoc} */
    @Override
    public void joinServer(GameProfile profile, String authenticationToken, String serverId) throws AuthenticationException {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("user", profile.getName());
        arguments.put("sessionId", authenticationToken);
        arguments.put("serverId", serverId);
        URL url = HttpAuthenticationService.concatenateURL(LegacyMinecraftSessionService.JOIN_URL, HttpAuthenticationService.buildQuery(arguments));
        try {
            String response = this.getAuthenticationService().performGetRequest(url);
            if (response.equals("OK")) return;
            throw new AuthenticationException(response);
        } catch (IOException e) {
            throw new AuthenticationUnavailableException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public GameProfile hasJoinedServer(GameProfile user, String serverId) throws AuthenticationUnavailableException {

        Map<String, Object> arguments = new HashMap<>();

        arguments.put("user",       user.getName());
        arguments.put("serverId",   serverId);

        URL url = HttpAuthenticationService.concatenateURL(LegacyMinecraftSessionService.CHECK_URL, HttpAuthenticationService.buildQuery(arguments));
        try {
            String response = this.getAuthenticationService().performGetRequest(url);
            return response.equals("YES") ? user : null;
        } catch (IOException e) {
            throw new AuthenticationUnavailableException(e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> getTextures(GameProfile profile) {
        return new HashMap<>();
    }

    /** {@inheritDoc} */
    @Override
    public LegacyAuthenticationService getAuthenticationService() {
        return (LegacyAuthenticationService) super.getAuthenticationService();
    }
}

