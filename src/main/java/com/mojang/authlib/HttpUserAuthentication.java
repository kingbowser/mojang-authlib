package com.mojang.authlib;

/**
 * BaseUserAuthentication implementation that uses an Http-based AuthenticationService implementation as its back-end
 * @see com.mojang.authlib.BaseUserAuthentication
 * @see com.mojang.authlib.HttpAuthenticationService
 */
public abstract class HttpUserAuthentication
              extends BaseUserAuthentication {

    /**
     * Bridge constructor.
     *
     * @param authenticationService Http-based authentication service implementation
     */
    protected HttpUserAuthentication(HttpAuthenticationService authenticationService) {
        super(authenticationService);
    }

    /** {@inheritDoc} */
    @Override
    public HttpAuthenticationService getAuthenticationService() {
        return (HttpAuthenticationService) super.getAuthenticationService();
    }
}

