package com.mojang.authlib.yggdrasil.response;

import com.mojang.authlib.GameProfile;

/**
 * Response from the service when a refresh request is granted
 * @see com.mojang.authlib.yggdrasil.request.RefreshRequest
 */
public class RefreshResponse
        extends Response {
    private String accessToken;
    private String clientToken;
    private GameProfile selectedProfile;
    private GameProfile[] availableProfiles;
    private User user;

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getClientToken() {
        return this.clientToken;
    }

    public GameProfile[] getAvailableProfiles() {
        return this.availableProfiles;
    }

    public GameProfile getSelectedProfile() {
        return this.selectedProfile;
    }

    public User getUser() {
        return this.user;
    }
}

