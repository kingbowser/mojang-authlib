package com.mojang.authlib.yggdrasil.response;

import com.mojang.authlib.GameProfile;

/**
 * Response to a successful authentication request
 * @see com.mojang.authlib.yggdrasil.request.AuthenticationRequest
 */
public class AuthenticationResponse
        extends Response {

    private String accessToken;
    private String clientToken;

    /**
     * Current profile
     */
    private GameProfile selectedProfile;

    /**
     * Available user profiles
     */
    private GameProfile[] availableProfiles;

    /**
     * Authenticated user
     */
    private User user;

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getClientToken() {
        return this.clientToken;
    }

    public GameProfile[] getAvailableProfiles() {
        return this.availableProfiles;
    }

    public GameProfile getSelectedProfile() {
        return this.selectedProfile;
    }

    public User getUser() {
        return this.user;
    }
}

