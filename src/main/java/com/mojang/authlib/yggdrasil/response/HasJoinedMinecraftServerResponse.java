package com.mojang.authlib.yggdrasil.response;

import com.mojang.authlib.ProfileProperty;

import java.util.List;

/**
 * Remote service response sent to the client when it is granted permission to join a server
 * @see com.mojang.authlib.yggdrasil.request.JoinMinecraftServerRequest
 */
public class HasJoinedMinecraftServerResponse
     extends Response {
    private String id;
    private List<ProfileProperty> properties;

    public String getId() {
        return this.id;
    }

    public List<ProfileProperty> getProperties() {
        return this.properties;
    }
}

