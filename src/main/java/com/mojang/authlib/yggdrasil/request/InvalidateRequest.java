package com.mojang.authlib.yggdrasil.request;

import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

/**
 * GSON Model for transmitting the intent to cancel authentication to the remote service
 */
public class InvalidateRequest {

    private String accessToken;
    private String clientToken;

    public InvalidateRequest(YggdrasilUserAuthentication authenticationService) {
        this.accessToken = authenticationService.getAuthenticatedToken();
        this.clientToken = authenticationService.getAuthenticationService().getClientToken();
    }
}

