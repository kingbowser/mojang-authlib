package com.mojang.authlib.yggdrasil.request;

import com.mojang.authlib.Agent;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

/**
 * GSON Model for transmitting information about an authentication request to the remote service
 */
public class AuthenticationRequest {

    /*
     * Serialized by GSON
     */
    private Agent   agent;
    private String  username;
    private String  password;
    private String  clientToken;
    private boolean requestUser = true;

    public AuthenticationRequest(YggdrasilUserAuthentication authenticationService, String username, String password) {
        this.agent = authenticationService.getAgent();
        this.username = username;
        this.clientToken = authenticationService.getAuthenticationService().getClientToken();
        this.password = password;
    }
}

