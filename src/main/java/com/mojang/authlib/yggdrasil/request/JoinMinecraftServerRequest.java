package com.mojang.authlib.yggdrasil.request;

/**
 * GSON Model for transmitting the intent to join a server to the remote service
 */
public class JoinMinecraftServerRequest {
    public String accessToken;
    public String selectedProfile;
    public String serverId;
}

