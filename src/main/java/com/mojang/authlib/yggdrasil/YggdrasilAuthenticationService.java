package com.mojang.authlib.yggdrasil;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mojang.authlib.Agent;
import com.mojang.authlib.HttpAuthenticationService;
import com.mojang.authlib.UserAuthentication;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import com.mojang.authlib.exceptions.UserMigratedException;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.response.Response;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.Proxy;
import java.net.URL;

/**
 * Yggdrasil HTTP-Based authentication service implementation
 */
public class YggdrasilAuthenticationService
     extends HttpAuthenticationService {
    private final String clientToken;
    private final Gson gson = new Gson();

    public YggdrasilAuthenticationService(Proxy proxy, String clientToken) {
        super(proxy);
        this.clientToken = clientToken;
    }

    /** {@inheritDoc} */
    @Override
    public UserAuthentication createUserAuthentication(Agent agent) {
        return new YggdrasilUserAuthentication(this, agent);
    }

    /** {@inheritDoc} */
    @Override
    public MinecraftSessionService createMinecraftSessionService() {
        return new YggdrasilMinecraftSessionService(this);
    }

    /**
     * Make a request the Yggdrasil remote service
     *
     * @param url       Yggdrasil remote service request receiver
     * @param input     Request model
     * @param classOfT  Request class
     * @param <T>       Desired response type
     * @return          Response
     * @throws AuthenticationException
     */
    protected <T extends Response> T makeRequest(URL url, Object input, Class<T> classOfT) throws AuthenticationException {
        try {
            String jsonResult = input == null ? this.performGetRequest(url) : this.performPostRequest(url, this.gson.toJson(input), "application/json");
            T result = this.gson.fromJson(jsonResult, classOfT);
            if (result == null) {
                return null;
            }
            if (!StringUtils.isNotBlank(result.getError())) return result;
            if ("UserMigratedException".equals(result.getCause())) {
                throw new UserMigratedException(result.getErrorMessage());
            }
            if (!result.getError().equals("ForbiddenOperationException"))
                throw new AuthenticationException(result.getErrorMessage());
            throw new InvalidCredentialsException(result.getErrorMessage());
        } catch (IOException | IllegalStateException | JsonParseException e) {
            throw new AuthenticationUnavailableException("Cannot contact authentication server", e);
        }
    }

    /**
     * Get the client token assigned by the service
     *
     * @return cient token
     */
    public String getClientToken() {
        return this.clientToken;
    }
}

