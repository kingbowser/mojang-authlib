package com.mojang.authlib.yggdrasil;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.HttpAuthenticationService;
import com.mojang.authlib.ProfileProperty;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.minecraft.HttpMinecraftSessionService;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.yggdrasil.request.JoinMinecraftServerRequest;
import com.mojang.authlib.yggdrasil.response.HasJoinedMinecraftServerResponse;
import com.mojang.authlib.yggdrasil.response.MinecraftTexturesPayload;
import com.mojang.authlib.yggdrasil.response.Response;
import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * Yggdrasil implementation for minecraft
 */
public class YggdrasilMinecraftSessionService
     extends HttpMinecraftSessionService {

    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Base URL for minecraft Yggdrasil service
     */
    private static final String BASE_URL = "https://sessionserver.mojang.com/session/minecraft/";

    /**
     * Remote function called when transmitting the intent to join a server
     */
    private static final URL JOIN_URL = HttpAuthenticationService.constantURL("https://sessionserver.mojang.com/session/minecraft/join");

    /**
     * Remote function called when checking for join authorization
     */
    private static final URL CHECK_URL = HttpAuthenticationService.constantURL("https://sessionserver.mojang.com/session/minecraft/hasJoined");

    /**
     * Service public key
     */
    private final PublicKey publicKey;

    /**
     * Request serializer
     */
    private final Gson gson = new Gson();

    /**
     * Default constructor
     *
     * @param authenticationService Yggdrasil implementation
     */
    protected YggdrasilMinecraftSessionService(YggdrasilAuthenticationService authenticationService) {
        super(authenticationService);
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(IOUtils.toByteArray(YggdrasilMinecraftSessionService.class.getResourceAsStream("/yggdrasil_session_pubkey.der")));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            this.publicKey = keyFactory.generatePublic(spec);
        } catch (Exception e) {
            throw new Error("Missing/invalid yggdrasil public key!");
        }
    }

    /** {@inheritDoc} */
    @Override
    public void joinServer(GameProfile profile, String authenticationToken, String serverId) throws AuthenticationException {
        JoinMinecraftServerRequest request = new JoinMinecraftServerRequest();
        request.accessToken = authenticationToken;
        request.selectedProfile = profile.getId();
        request.serverId = serverId;
        this.getAuthenticationService().makeRequest(YggdrasilMinecraftSessionService.JOIN_URL, request, Response.class);
    }

    /** {@inheritDoc} */
    @Override
    public GameProfile hasJoinedServer(GameProfile user, String serverId) throws AuthenticationUnavailableException {
        Map<String, Object> arguments = new HashMap<>();

        arguments.put("username", user.getName());
        arguments.put("serverId", serverId);

        URL url = HttpAuthenticationService.concatenateURL(YggdrasilMinecraftSessionService.CHECK_URL, HttpAuthenticationService.buildQuery(arguments));
        try {
            HasJoinedMinecraftServerResponse response = this.getAuthenticationService().makeRequest(url, null, HasJoinedMinecraftServerResponse.class);

            if (response == null || response.getId() == null) return null;

            GameProfile result = new GameProfile(response.getId(), user.getName());

            if (response.getProperties() == null) return result;

            for (ProfileProperty property : response.getProperties()) {
                result.getProperties().put(property.getName(), property);
            }

            return result;
        } catch (AuthenticationUnavailableException e) {
            throw e;
        } catch (AuthenticationException e) {
            return null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> getTextures(GameProfile profile) {
        MinecraftTexturesPayload result;
        ProfileProperty textureProperty = profile.getProperties().get("textures");
        if (textureProperty == null) {
            return new HashMap<>();
        }
        if (!textureProperty.hasSignature()) {
            YggdrasilMinecraftSessionService.LOGGER.error("Signature is missing from textures payload");
            return new HashMap<>();
        }
        if (!textureProperty.isSignatureValid(this.publicKey)) {
            YggdrasilMinecraftSessionService.LOGGER.error("Textures payload has been tampered with (signature invalid)");
            return new HashMap<>();
        }
        try {
            String json = new String(Base64.decodeBase64(textureProperty.getValue()), Charsets.UTF_8);
            result = (MinecraftTexturesPayload) this.gson.fromJson(json, (Class) MinecraftTexturesPayload.class);
        } catch (JsonParseException e) {
            YggdrasilMinecraftSessionService.LOGGER.error("Could not decode textures payload", e);
            return new HashMap<>();
        }
        if (!(result.getProfileId() != null && result.getProfileId().equals(profile.getId()))) {
            YggdrasilMinecraftSessionService.LOGGER.error("Decrypted textures payload was for another user (expected id {} but was for {})", profile.getId(), result.getProfileId());
            return new HashMap<>();
        }
        if (!(result.getProfileName() != null && result.getProfileName().equals(profile.getName()))) {
            YggdrasilMinecraftSessionService.LOGGER.error("Decrypted textures payload was for another user (expected name {} but was for {})", profile.getName(), result.getProfileName());
            return new HashMap<>();
        }
        return result.getTextures() == null ? new HashMap<MinecraftProfileTexture.Type, MinecraftProfileTexture>() : result.getTextures();
    }

    /** {@inheritDoc} */
    @Override
    public YggdrasilAuthenticationService getAuthenticationService() {
        return (YggdrasilAuthenticationService) super.getAuthenticationService();
    }
}

