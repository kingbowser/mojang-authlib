package com.mojang.authlib;

import org.apache.commons.codec.binary.Base64;

import java.security.*;

/**
 * Represents a property of a user profile
 */
public class ProfileProperty {

    private final String name;
    private final String value;
    private final String signature;

    /**
     * Create an unsigned user property
     *
     * @param value value
     * @param name  name
     */
    public ProfileProperty(String name, String value) {
        this(name, value, null);
    }

    /**
     * Create a signed user property
     *
     * @param name      Name
     * @param value     Value
     * @param signature Base64 encoded signature
     */
    public ProfileProperty(String name, String value, String signature) {
        this.name       = name;
        this.value      = value;
        this.signature  = signature;
    }

    /**
     * Get the property name
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get the property value
     *
     * @return value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Get the property's signature
     *
     * @return signature
     */
    public String getSignature() {
        return this.signature;
    }

    /**
     * Returns true if the property is signed
     *
     * @return whether or not the property is signed
     */
    public boolean hasSignature() {
        return this.signature != null;
    }

    /**
     * Validate the property signature using the specified public key
     *
     * @param publicKey public key
     * @return signature validity
     */
    public boolean isSignatureValid(PublicKey publicKey) {
        try {
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initVerify(publicKey);
            signature.update(this.value.getBytes());
            return signature.verify(Base64.decodeBase64(this.signature));
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
        }
        return false;
    }
}

