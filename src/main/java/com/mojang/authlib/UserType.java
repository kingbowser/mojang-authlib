package com.mojang.authlib;

import java.util.HashMap;
import java.util.Map;

/**
 * User type Enumeration
 */
public enum UserType {

    /**
     * Describes an account that is currently managed by the legacy authentication service
     */
    LEGACY("legacy"),

    /**
     * Describes an account that is managed by the modern authentication service.
     */
    MOJANG("mojang");

    private static final Map<String, UserType> BY_NAME = new HashMap<>();

    private final String name;

    private UserType(String name) {
        this.name = name;
    }

    public static UserType byName(String name) {
        return UserType.BY_NAME.get(name.toLowerCase());
    }

    public String getName() {
        return this.name;
    }

    static {
        for (UserType type : UserType.values()) {
            UserType.BY_NAME.put(type.name, type);
        }
    }
}

