package com.mojang.authlib;

/**
 * Describes the requesting game
 * Currently Minecraft ({@link Agent#MINECRAFT}) or Scrolls ({@link Agent#SCROLLS})
 */
public enum Agent {
	MINECRAFT("Minecraft", 1),
	SCROLLS("Scrolls", 1);
	
	private final String name;
	private final int version;
	
	private Agent(String name, int version) {
		this.name = name;
		this.version = version;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getVersion() {
		return this.version;
	}
	
	@Override
	public String toString() {
		return "Agent{name='" + this.getName() + "'" + ", version=" + this.getVersion() + "}";
	}
}