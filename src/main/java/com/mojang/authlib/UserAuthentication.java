package com.mojang.authlib;

import com.mojang.authlib.exceptions.AuthenticationException;

import java.util.Collection;
import java.util.Map;

/**
 * Contract for a service that provides user profile data for credentials.
 */
public interface UserAuthentication {

    /**
     * Whether or not the service is accepting authentication requests, or whether or not the client may make a request.
     *
     * @return can log in
     */
    public boolean canLogIn();

    /**
     * Log in to the service using credentials provided either at construction, or by an implementation-specific method at runtime.
     *
     * @throws AuthenticationException if any error should occur during the authentication process
     */
    public void logIn() throws AuthenticationException;

    /**
     * Un-authenticate from the service, once again using the credentials provided at construction, or by an implementation-specific method at runtime.
     */
    public void logOut();

    /**
     * Returns true when the instance has made a successful request and has a valid session
     *
     * @return logged in
     */
    public boolean isLoggedIn();

    /**
     * Returns true when the authenticated user is authorised to join servers.
     * In the case of Minecraft, this is true when the user has purchased a premium account.
     *
     * @return can play online (multiplayer)
     */
    public boolean canPlayOnline();

    /**
     * Returns a list of profiles (user names) saved by service.
     *
     * @return profiles
     */
    public GameProfile[] getAvailableProfiles();

    /**
     * Get the profile that is currently authenticated with the service
     *
     * @return current profile
     */
    public GameProfile getSelectedProfile();

    /**
     * Authenticate to the service with a profile
     *
     * @param gameProfile profile with which to authenticate
     * @throws AuthenticationException when any error should occur during the authentication process
     */
    public void selectGameProfile(GameProfile gameProfile) throws AuthenticationException;

    /**
     * Load GameProfiles from storage
     *
     * @param storage storage or cache of GameProfiles
     */
    public void loadFromStorage(Map<String, String> storage);

    /**
     * Convert known GameProfiles in to a map for storage
     *
     * @return userId/userName map
     */
    public Map<String, String> saveForStorage();

    /**
     * Set the username used in authentication
     *
     * @param userName username with which to authenticate to the service
     */
    public void setUsername(String userName);

    /**
     * Set the password used in authentication
     *
     * @param userPassword password with which to authenticate
     */
    public void setPassword(String userPassword);

    /**
     * Get the current authentication token used to safely identify the session to outside services and other processes
     *
     * @return authentication token
     */
    public String getAuthenticatedToken();

    /**
     * Get the current user ID with which the service has been authenticated to
     *
     * @return current user ID
     */
    public String getUserID();

    /**
     * Get the current user properties
     *
     * @return current user properties
     */
    public Map<String, Collection<String>> getUserProperties();

    /**
     * Get the current user's account type (legacy minecraft or mojang)
     *
     * @see com.mojang.authlib.UserType
     * @return account type
     */
    public UserType getUserType();
}

