package com.mojang.authlib;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Information about a service user.
 * GameProfile is a model containing a user id, a user name, and user properties.
 */
public class GameProfile {

    /**
     * User UUID
     */
    private final String id;

    /**
     * User Name
     */
    private final String name;

    /**
     * User properties
     */
    private final Map<String, ProfileProperty> properties = new HashMap<>();

    /**
     * True only if the user has a legacy (un-migrated minecraft) account.
     */
    private boolean legacy;

    /**
     * Default constructor.
     *
     * @param id    User UUID
     * @param name  User Name
     */
    public GameProfile(String id, String name) {
        if (StringUtils.isBlank(id) && StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name and ID cannot both be blank");
        }
        this.id     = id;
        this.name   = name;
    }

    /**
     * Get the Profile's User UUID
     *
     * @return UUID of profile User
     */
    public String getId() {
        return this.id;
    }

    /**
     * Get the Profile's User Name
     *
     * @return Name of profile User
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get the Profile's User Properties
     *
     * @return Properties of profile User
     */
    public Map<String, ProfileProperty> getProperties() {
        return this.properties;
    }

    /**
     * Returns true when the profile has UUID and Name
     *
     * @return completion status
     */
    public boolean isComplete() {
        return StringUtils.isNotBlank(this.getId()) && StringUtils.isNotBlank(this.getName());
    }

    /** {@inheritDoc} */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        GameProfile that = (GameProfile) o;

        return this.id.equals(that.id) && this.name.equals(that.name);
    }

    /** {@inheritDoc} */
    public int hashCode() {
        int result = this.id.hashCode();
        result = 31 * result + this.name.hashCode();
        return result;
    }

    /** {@inheritDoc} */
    public String toString() {
        return "GameProfile{id='" + this.id + '\'' + ", name='" + this.name + '\'' + '}';
    }

    /**
     * Returns true when the user is a legacy (minecraft) user
     *
     * @return legacy status
     */
    public boolean isLegacy() {
        return this.legacy;
    }
}

