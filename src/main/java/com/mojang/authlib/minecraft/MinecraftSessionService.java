package com.mojang.authlib.minecraft;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;

import java.util.Map;

/**
 * Defines contract for a session management service that handles client-server interaction.
 * This service also must provide Skin and Cape texture information.
 */
public interface MinecraftSessionService {

    /**
     * Inform the remote service of the intent to join a specified server.
     *
     * @param gameProfile User profile which is to join the server.
     * @param server      Server address.
     * @param serverId    Server id.
     * @throws AuthenticationException if a malformed response is received.
     */
    public void joinServer(GameProfile gameProfile, String server, String serverId) throws AuthenticationException;

    /**
     * Returns a profile instance for the player after having connected to a server.
     * This will return null if the player has not connected.
     *
     * @see com.mojang.authlib.GameProfile
     * @param gameProfile User profile (to check server login status)
     * @param serverId    Server id
     * @return            User profile pos-login
     * @throws AuthenticationUnavailableException if the remote service is not responding to requests
     */
    public GameProfile hasJoinedServer(GameProfile gameProfile, String serverId) throws AuthenticationUnavailableException;

    /**
     * Get the player (cape and skin) textures to be used with the specified GameProfile
     *
     * @see com.mojang.authlib.GameProfile
     * @param  gameProfile GameProfile instance, this is a User.
     * @return Skin and/or Cape textures
     */
    public Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> getTextures(GameProfile gameProfile);
}

