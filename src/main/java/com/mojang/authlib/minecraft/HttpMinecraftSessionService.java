package com.mojang.authlib.minecraft;

import com.mojang.authlib.HttpAuthenticationService;

/**
 * BaseMinecraftSessionService that operates by means of an HttpAuthenticationService implementation
 */
public abstract class HttpMinecraftSessionService
        extends BaseMinecraftSessionService {

    /**
     * Default constructor (for HTTP SessionService implementation)
     *
     * @see com.mojang.authlib.HttpAuthenticationService
     * @param authenticationService HttpAuthenticationService Implementation
     */
    protected HttpMinecraftSessionService(HttpAuthenticationService authenticationService) {
        super(authenticationService);
    }

    /** {@inheritDoc} */
    @Override
    public HttpAuthenticationService getAuthenticationService() {
        return (HttpAuthenticationService) super.getAuthenticationService();
    }
}

