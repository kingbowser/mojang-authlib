package com.mojang.authlib.minecraft;

import com.mojang.authlib.AuthenticationService;

/**
 * Session service parent class.
 * Adds AuthenticationService implementation access.
 */
public abstract class BaseMinecraftSessionService
           implements MinecraftSessionService {

    /**
     * AuthenticationService implementation
     */
    private final AuthenticationService authenticationService;

    /**
     * Default constructor
     *
     * @param authenticationService AuthenticationService implementation
     */
    protected BaseMinecraftSessionService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    /**
     * Get the companion AuthenticationService implementation.
     *
     * @return AuthenticationService implementation
     */
    public AuthenticationService getAuthenticationService() {
        return this.authenticationService;
    }
}

