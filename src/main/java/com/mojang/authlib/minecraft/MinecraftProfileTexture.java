package com.mojang.authlib.minecraft;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Information about a user's Skin/Cape textures
 * @see com.mojang.authlib.GameProfile
 */
public class MinecraftProfileTexture {

    /** Path to the physical texture resource */
    private final String url;

    /**
     * Default constructor
     *
     * @param url texture location
     */
    public MinecraftProfileTexture(String url) {
        this.url = url;
    }

    /**
     * Get the path to the texture resource
     *
     * @return path to the texture resource
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * Get the name of the texture resource file
     *
     * @return resource file name
     */
    public String getHash() {
        return FilenameUtils.getBaseName(this.url);
    }


    /** {@inheritDoc} */
    public String toString() {
        return new ToStringBuilder(this).append("url", this.url).append("hash", this.getHash()).toString();
    }

    /**
     * Texture type enumeration
     */
    public static enum Type {

        /**
         * Minecraft player Skin ordinal
         */
        SKIN,

        /**
         * Minecraft player Cape ordinal
         */
        CAPE

    }

}

