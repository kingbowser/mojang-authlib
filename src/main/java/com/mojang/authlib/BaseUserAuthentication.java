package com.mojang.authlib;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseUserAuthentication
        implements UserAuthentication {

    /**
     * Key used to store the display name (of the profile)
     */
    protected static final String STORAGE_KEY_PROFILE_NAME  = "displayName";

    /**
     * Key used to store the UUID of the current user
     */
    protected static final String STORAGE_KEY_PROFILE_ID    = "uuid";

    /**
     * Key used to store the username of the current user
     */
    protected static final String STORAGE_KEY_USER_NAME     = "username";

    /**
     * Key used to store the userId of the current user
     */
    protected static final String STORAGE_KEY_USER_ID       = "userid";

    /**
     * AuthenticationService implementation being used as the back-end
     */
    private final AuthenticationService authenticationService;

    /**
     * Current user properties (modifiable).
     * Cleared when {@link #logOut()} is invoked.
     */
    private final Map<String, Collection<String>> userProperties = new HashMap<>();

    /**
     * Current user ID
     */
    private String      userid;

    /**
     * Current user Name
     * Nullified when {@link #logOut()} is invoked
     */
    private String      username;

    /**
     * Current user Password
     * Nullified when {@link #logOut()} is invoked
     */
    private String      password;

    /**
     * Current user profile
     * Nullified when {@link #logOut()} is invoked (vis-a-vis {@link #setSelectedProfile(GameProfile)})
     */
    private GameProfile selectedProfile;

    /**
     * Current user type
     * Nullified when {@link #logOut()} is invoked (vis-a-vis {@link #setUserType(UserType)})
     */
    private UserType    userType;

    /**
     * Default constructor
     *
     * @param authenticationService AuthenticationService implementation to use as back-end
     */
    protected BaseUserAuthentication(AuthenticationService authenticationService) {
        Validate.notNull((Object) authenticationService);
        this.authenticationService = authenticationService;
    }

    /** {@inheritDoc} */
    @Override
    public boolean canLogIn() {
        return !this.canPlayOnline() && StringUtils.isNotBlank(this.getUsername()) && StringUtils.isNotBlank(this.getPassword());
    }

    /** {@inheritDoc} */
    @Override
    public void logOut() {
        this.password   = null;
        this.userid     = null;
        this.setSelectedProfile(null);
        this.getModifiableUserProperties().clear();
        this.setUserType(null);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isLoggedIn() {
        return this.getSelectedProfile() != null;
    }

    /** {@inheritDoc} */
    @Override
    public void setUsername(String userName) {
        if (this.isLoggedIn() && this.canPlayOnline()) {
            throw new IllegalStateException("Cannot change username whilst logged in & online");
        }
        this.username = userName;
    }

    /** {@inheritDoc} */
    @Override
    public void setPassword(String userPassword) {
        if (this.isLoggedIn() && this.canPlayOnline() && StringUtils.isNotBlank(userPassword)) {
            throw new IllegalStateException("Cannot set password whilst logged in & online");
        }
        this.password = userPassword;
    }

    /**
     * Get the current username
     *
     * @return current username
     */
    protected String getUsername() {
        return this.username;
    }

    /**
     * Get the current password
     *
     * @return current password
     */
    protected String getPassword() {
        return this.password;
    }

    /** {@inheritDoc} */
    @Override
    public void loadFromStorage(Map<String, String> storage) {
        this.logOut();
        this.setUsername(storage.get("username"));
        this.userid = storage.containsKey("userid") ? storage.get("userid") : this.username;
        if (!storage.containsKey("displayName") || !storage.containsKey("uuid")) return;
        this.setSelectedProfile(new GameProfile(storage.get("uuid"), storage.get("displayName")));
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, String> saveForStorage() {
        HashMap<String, String> result = new HashMap<>();
        if (this.getUsername() != null) {
            result.put("username", this.getUsername());
        }
        if (this.getUserID() != null) {
            result.put("userid", this.getUserID());
        } else if (this.getUsername() != null) {
            result.put("username", this.getUsername());
        }
        if (this.getSelectedProfile() == null) return result;
        result.put("displayName", this.getSelectedProfile().getName());
        result.put("uuid", this.getSelectedProfile().getId());
        return result;
    }

    /**
     * Get the selected user profile
     *
     * @param selectedProfile user profile
     */
    protected void setSelectedProfile(GameProfile selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    /** {@inheritDoc} */
    @Override
    public GameProfile getSelectedProfile() {
        return this.selectedProfile;
    }

    /** {@inheritDoc} */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.getClass().getSimpleName());
        result.append("{");
        if (this.isLoggedIn()) {
            result.append("Logged in as ");
            result.append(this.getUsername());
            if (this.getSelectedProfile() != null) {
                result.append(" / ");
                result.append(this.getSelectedProfile());
                result.append(" - ");
                if (this.canPlayOnline()) {
                    result.append("Online");
                } else {
                    result.append("Offline");
                }
            }
        } else {
            result.append("Not logged in");
        }
        result.append("}");
        return result.toString();
    }

    /**
     * Get the authentication back-end
     *
     * @return AuthenticationService implementation (as back-end)
     */
    public AuthenticationService getAuthenticationService() {
        return this.authenticationService;
    }

    /** {@inheritDoc} */
    @Override
    public String getUserID() {
        return this.userid;
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, Collection<String>> getUserProperties() {
        if (!this.isLoggedIn()) return Collections.unmodifiableMap(new HashMap<String, Collection<String>>());
        return Collections.unmodifiableMap(this.getModifiableUserProperties());
    }

    /**
     * Get the modifiable user prooperties
     *
     * @return modifiable user properties
     */
    protected Map<String, Collection<String>> getModifiableUserProperties() {
        return this.userProperties;
    }

    /** {@inheritDoc} */
    @Override
    public UserType getUserType() {
        if (!this.isLoggedIn()) return null;
        return this.userType == null ? UserType.LEGACY : this.userType;
    }

    /**
     * Set the type of the current user
     *
     * @param userType user type
     */
    protected void setUserType(UserType userType) {
        this.userType = userType;
    }

    /**
     * Set the id of the current user
     *
     * @param userId user id
     */
    protected void setUserid(String userId) {
        this.userid = userId;
    }
}

